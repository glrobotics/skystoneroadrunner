package org.firstinspires.ftc.teamcode.Team10785;
//I CHANGED getRobotAngle() BUT DIDNT CHANGE HOW DESIRED ANGLE IS USED
import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.dashboard.config.ValueProvider;
import com.acmerobotics.dashboard.config.variable.BasicVariable;
import com.acmerobotics.dashboard.config.variable.CustomVariable;
import com.acmerobotics.dashboard.telemetry.TelemetryPacket;
import com.acmerobotics.roadrunner.control.PIDCoefficients;
import com.acmerobotics.roadrunner.control.PIDFController;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.PIDFCoefficients;
import com.qualcomm.robotcore.util.RobotLog;

@Config
@TeleOp

public class Team10785TeleOp extends LinearOpMode {

    Robot10785 robot = new Robot10785();

    public Vision visionSystem;
    public String found;
    public int liftTarget;
    public int slideTarget;
    public int clawTarget;
    public int liftCount;
    public int startingLiftPos=0;
    private double startTimeTele = 0;
    public final int slideout = 411;
    public final int slidein = 0;
    public final int slideOutBlock=205;
    public boolean awaspressed = false;
    public boolean ywaspressed=false;
    public boolean bwaspressed=false;
    public boolean xwaspressed = false;
    public boolean rightBumperWasPressed=false;
    private float initRobotAngle;
    public float savedRobotAngle;
    public final int liftceil = 2226;
    public final int liftfloor = 0;
    public float tape1Target =0;
    public float tape2Target =0;
    public final float hookDown=1.0f;
    public final float hook2Down=0f;
    public final float hookUp=0.44f;
    public final float hook2Up=0.5f;
    public final float clawOpen=1.2f;
    public final float claw2Open=0f;
    public float claw2Close=1.05f;
    public final float clawClose=0.6f;
    public final float capstoneUp=0.1f;
    public final float capstoneDown=0.55f;
    public boolean capstonePosition=false;
    public boolean capstonePressed=false;

    private boolean speedModeHigh = true;
    private boolean speedModePressed = false;
    public int[] liftClearHeight={liftfloor,448,548,869,1115,1361,1610,1944};
    public int[] liftPlaceHeight={liftfloor,148,405,691,1034,1287,1490,1703};
    public int clearCount=0;

    private float totalCumulative, cumulativeError,correctionFactor,targetTangle;
    private FtcDashboard dashboard;
    private PIDFController turnPIDController;
    private PIDCoefficients turnPIDCoefficients=new PIDCoefficients();
    public double kP=0;
    public double kI=0;
    public double kD=0;
    private String catName;
    private CustomVariable catVar;
    private static final String PID_VAR_NAME = "TeleOp Turn PID";
    private boolean driveModePID=false;

    private void addPidVariable() {
        catName = getClass().getSimpleName();
        catVar = (CustomVariable) dashboard.getConfigRoot().getVariable(catName);

        if (catVar == null) {
            // this should never happen...
            catVar = new CustomVariable();
            dashboard.getConfigRoot().putVariable(catName, catVar);

            RobotLog.w("Unable to find top-level category %s", catName);
        }

        CustomVariable pidVar = new CustomVariable();
        CustomVariable claw2Var=new CustomVariable();
        pidVar.putVariable("kP", new BasicVariable<>(new ValueProvider<Double>() {
            @Override
            public Double get() {
                return kP;
            }

            @Override
            public void set(Double value) {
                kP=value;
                turnPIDCoefficients.kP=kP;
                turnPIDCoefficients.kI=kI;
                turnPIDCoefficients.kD=kD;
                turnPIDController=new PIDFController(turnPIDCoefficients,0,0,0);

            }
        }));
        pidVar.putVariable("kI", new BasicVariable<>(new ValueProvider<Double>() {
            @Override
            public Double get() {
                return kI;
            }

            @Override
            public void set(Double value) {
                kI=value;
                turnPIDCoefficients.kP=kP;
                turnPIDCoefficients.kI=kI;
                turnPIDCoefficients.kD=kD;
                turnPIDController=new PIDFController(turnPIDCoefficients,0,0,0);

            }
        }));
        claw2Var.putVariable("claw2", new BasicVariable<>(new ValueProvider<Double>() {
            @Override
            public Double get() {
                return (double)claw2Close;
            }

            @Override
            public void set(Double value) {
                claw2Close=value.floatValue();
            }
        }));






        pidVar.putVariable("kD", new BasicVariable<>(new ValueProvider<Double>() {
            @Override
            public Double get() {
                return kD;
            }

            @Override
            public void set(Double value) {
                kD=value;
                turnPIDCoefficients.kP=kP;
                turnPIDCoefficients.kI=kI;
                turnPIDCoefficients.kD=kD;
                turnPIDController=new PIDFController(turnPIDCoefficients,0,0,0);

            }
        }));

        catVar.putVariable(PID_VAR_NAME, pidVar);
        dashboard.updateConfig();
    }

    private void removePidVariable() {
        if (catVar.size() > 1) {
            catVar.removeVariable(PID_VAR_NAME);
        } else {
            dashboard.getConfigRoot().removeVariable(catName);
        }
        dashboard.updateConfig();
    }

    @Override
    public void runOpMode(){
        robot.init(hardwareMap);
        turnPIDCoefficients.kP=kP;
        turnPIDCoefficients.kI=kI;
        turnPIDCoefficients.kD=kD;
        turnPIDController=new PIDFController(turnPIDCoefficients,0,0,0);
        turnPIDController.setOutputBounds(-1,1);
        turnPIDController.setInputBounds(-180,180);
        telemetry.addData("Stauts","Initialized 1");
        telemetry.update();
        dashboard = FtcDashboard.getInstance();
        dashboard.setTelemetryTransmissionInterval(25);
        addPidVariable();
        robot.claw.setPosition(clawClose);
        robot.claw2.setPosition(claw2Close);
        robot.hook.setPosition(hookUp);  // close hook 1
        robot.hook2.setPosition(hook2Up);  // close hook 2
        robot.capstone.setPosition(capstoneUp);
        while (!opModeIsActive() && !isStopRequested()) {
            telemetry.addData("Lift Position", "%d ", robot.lift.getCurrentPosition());
            telemetry.addData("Slide", "%d ", robot.slide.getCurrentPosition());
            telemetry.addData("Rotation", "%f ", robot.getRobotAngle());
            telemetry.update();
        }

        startingLiftPos=robot.lift.getCurrentPosition();
        liftTarget=startingLiftPos;
//        robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift.setTargetPosition(robot.lift.getCurrentPosition());
//        robot.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.lift.setPower(1);

//        robot.slide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.slide.setTargetPosition(robot.slide.getCurrentPosition());
//        robot.slide.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slide.setPower(1);
        slideTarget=robot.slide.getTargetPosition();
        liftTarget = startingLiftPos;
        liftCount = 0;
        savedRobotAngle=robot.getRobotAngle();

        robot.tape1.setTargetPosition(0);
        robot.tape2.setTargetPosition(0);
        robot.tape1.setPower(1);
        robot.tape2.setPower(1);
        robot.claw.setPosition(clawOpen);
        robot.claw2.setPosition(claw2Open);

        while(opModeIsActive()) {
            // Convert joysticks to desired motion.
            if (gamepad1.right_stick_button && !speedModePressed) {
                speedModePressed = true;
                speedModeHigh = !speedModeHigh;
            }
            if (!gamepad1.right_stick_button) {
                speedModePressed = false;
            }

            if (gamepad1.dpad_left) {
                tape1Target += 2;
                //if (tape1Target>120)
                //    tape1Target=120;
            } else if (gamepad1.dpad_right) {
                tape1Target -= 2;
                //if (tape1Target<-10)
                //    tape1Target=-10;
            }
            robot.tape1.setTargetPosition((int) (tape1Target * robot.countsPerInchTape));


            if (gamepad1.b) {
                tape2Target += 2;
                //if (tape2Target>120)
                //    tape2Target=120;
            } else if (gamepad1.x) {
                tape2Target -= 2;
                //if (tape2Target<-10)
                //    tape2Target=-10;
            }
            robot.tape2.setTargetPosition((int) (tape2Target * robot.countsPerInchTape));


            double robotRad = robot.getRobotAngleRad() + robot.startingAngle;

            double x = ((gamepad1.left_stick_x * Math.cos(robotRad)) - (gamepad1.left_stick_y * Math.sin(robotRad)));
            double y = ((gamepad1.left_stick_y * Math.cos(robotRad)) + (gamepad1.left_stick_x * Math.sin(robotRad)));
            x=x+((0.5f*gamepad1.right_trigger)-(0.5f*gamepad1.left_trigger));


            if (!speedModeHigh) {
                x /= 3.0;
                y /= 3.0;
            }

            if (gamepad2.right_stick_button) {
                robot.claw.setPosition(0);
                robot.claw2.setPosition(claw2Close);
                liftTarget=liftClearHeight[1];
                robot.slide.setTargetPosition(0);
            }
            if (Math.abs(y) > Math.abs(x)) {
                driveModePID = false;
            } else {
                if (driveModePID == false) {
                    driveModePID = true;
                        savedRobotAngle = robot.getRobotAngle();

                }
            }
            // POV Control
            Mecanum.Motion motion;
            if (Math.abs(gamepad1.right_stick_x) > 0.05) {
                motion = Mecanum.joystickToMotion(x, y, gamepad1.right_stick_x, gamepad1.right_stick_y);

                    savedRobotAngle = robot.getRobotAngle();

                correctionFactor = 0;
                cumulativeError = 0;
                totalCumulative = 0;
            } else if ((Math.abs(x) > 0.05) || Math.abs(y) > 0.05) {
                turnPIDController.setTargetPosition(0);
                if (driveModePID) {
                    targetTangle=(savedRobotAngle-robot.getRobotAngle()+180)%360-180;
                        motion = Mecanum.joystickToMotion(x, y, turnPIDController.update(targetTangle), 0);
                } else {
                    motion = Mecanum.joystickToMotion(x, y, 0, 0);
                }
            } else {
                motion = Mecanum.joystickToMotion(0, 0, 0, 0);
            }
            TelemetryPacket packet = new TelemetryPacket();
            packet.put("Desired Angle: ", savedRobotAngle);
            packet.put("Actual Angle: ", robot.getRobotAngle());

            dashboard.sendTelemetryPacket(packet);

            /*
            // Non Adjusted Motion
            Mecanum.Motion motion = Mecanum.joystickToMotion(
                    gamepad1.left_stick_x, gamepad1.left_stick_y,
                    gamepad1.right_stick_x, gamepad1.right_stick_y);
*/


            // Convert desired motion to wheel powers, with power clamping.

            motion.vTheta = motion.vTheta * 0.8;
            Mecanum.Wheels wheels = Mecanum.motionToWheels(motion);
            wheels = Mecanum.Wheels.scaleWheelPower();
            // wheels=Mecanum.Wheels.scaleWheelPowerTwo();
            robot.setWheelSpeeds(wheels.frontLeft, wheels.backLeft, wheels.frontRight, wheels.backRight);


            /*/
             * The paramater "liftTarget" is the target that we want to move the lift to
             * When we hold down the button, we increase the target by 1, but also tell the robot to
             * move to the position, creating motion along the lift. This is more consistent than
             * robot.lift.setpower(int power); because it creates continuous motion along the lift.
             * Using robot.lift.setpower(int power); causes the lift to move in multiple tiny
             * increments opposed to one big one, which takes a LONG time compared to using the
             * "target" way of doing this.
            /*/

            //lift
            {
                if (gamepad2.left_stick_y < -0.2) {
                    liftTarget += 10;
                } else if (gamepad2.left_stick_y > 0.2) {
                    liftTarget -= 10;

                }

                if (liftTarget > liftceil) {
                   liftTarget = liftceil;
                }
                if (liftTarget < liftfloor) {
                    liftTarget = liftfloor;
                }


                robot.lift.setTargetPosition(liftTarget);

            }//end lift

            //slide
            {
                if (gamepad2.dpad_left) {
                    slideTarget = slideOutBlock;
                }

                if (gamepad2.dpad_right) {
                    slideTarget = 0;
                }

                /*if (gamepad2.left_stick_x > 0.2) {
                    slidetarget += 1;
                } else if (gamepad2.left_stick_x < -0.2) {
                    slidetarget -= 1;

                }*/

                if (slideTarget > slideout) {
                    slideTarget = slideout; //is zero out or in?
                }
                if (slideTarget < slidein) {
                    slideTarget = slidein;
                }
                robot.slide.setTargetPosition(slideTarget);
            }//end slide

            //claw2
            {
                if (gamepad2.dpad_up) {//UP = OPEN
                    robot.claw2.setPosition(claw2Open);
                    robot.claw.setPosition(clawOpen);
                }
                if (gamepad2.dpad_down) {//DOWN = CLOSED
                    robot.claw2.setPosition(claw2Close);
                    robot.claw.setPosition(clawClose);
                    slideTarget = slideOutBlock;
                }

            }//end claw




            //buttons
            {

                if (clearCount>6){
                    clearCount=6;
                }
                if (clearCount<0){
                    clearCount=0;
                }
                if ((gamepad2.x)&&(!xwaspressed)) {//in then down
                    xwaspressed = true;
                    clearCount=0;
                    xwaspressed=false;
                    liftTarget=0;
                    robot.claw.setPosition(clawOpen);
                    robot.claw2.setPosition(claw2Open);
                }
                if(!xwaspressed){
                    xwaspressed=false;
                }

                if (gamepad2.left_trigger > .5) {
                        liftTarget = 133;
                        clearCount=0;
                }
                if ((gamepad2.a)&&(!awaspressed)) {
                    awaspressed=true;
                    clearCount--;
                    if (clearCount>6){
                        clearCount=6;
                    }
                    if (clearCount<0){
                        clearCount=0;
                    }
                    liftTarget = liftClearHeight[clearCount];
                }
                if(!gamepad2.a){
                    awaspressed=false;
                }
                if ((gamepad2.b)&&(robot.lift.getCurrentPosition()>300)&&(!bwaspressed)) {
                    bwaspressed=true;
                    if (clearCount>6){
                        clearCount=6;
                    }
                    if (clearCount<0){
                        clearCount=0;
                    }
                    liftTarget = liftPlaceHeight[clearCount];
                }
                if(!gamepad2.b){
                    bwaspressed=false;
                }
                if ((gamepad2.y)&&(!ywaspressed)) {
                    ywaspressed=true;
                    clearCount++;
                    if (clearCount>6){
                        clearCount=6;
                    }
                    if (clearCount<0){
                        clearCount=0;
                    }
                    liftTarget = liftClearHeight[clearCount];
                }
                if(!gamepad2.y){
                    ywaspressed=false;
                }
                if (gamepad2.right_stick_y > .9) {
                    slideTarget = slideOutBlock;
                }

                if (slideTarget > slideout) {
                    slideTarget = slideout;
                }
                if (slideTarget < slidein) {
                    slideTarget = slidein;
                }


                robot.slide.setTargetPosition(slideTarget);
            }//end buttons

            //pin release
            {
                if ((gamepad2.right_trigger > .5)&&(!capstonePressed)) {
                    capstonePressed=true;
                    if(capstonePosition) {
                        robot.capstone.setPosition(capstoneDown);
                        capstonePosition=false;
                    }
                    else{
                        robot.capstone.setPosition(capstoneUp);
                        capstonePosition=true;
                    }
                }
                if (gamepad2.right_trigger<.2) {
                    capstonePressed = false;
                }



            }// end of pin release

            { //hook

                if (gamepad1.right_bumper) {
                    robot.hook.setPosition(hookDown);  // close hook 1
                    robot.hook2.setPosition(hook2Down); } // close hook 2

                if (gamepad1.left_bumper) {
                    robot.hook.setPosition(hookUp);  //open hook 1
                    robot.hook2.setPosition(hook2Up); } //open hook  2

            } // end of hook



            robot.lift.setTargetPosition(liftTarget);

            telemetry.addData("Robot Angle: ","%f",robot.getRobotAngle());
            telemetry.addData("Robot Target Angle: ","%f",targetTangle);
            telemetry.addData("Status","Running");
            telemetry.addData("MoveStep","%s",robot.moveStep.name());
            telemetry.addData("Starting Arm Postition", "%d", startingLiftPos);
            telemetry.addData("Found","%s",found);
            telemetry.addData("Velocity: ","%f",robot.getVelocity());
            telemetry.addData("Velocity Error: ","%f",robot.getVelocityError());
            telemetry.addData("Left Joystick Command: ","%f",wheels.frontLeft);
            telemetry.addData("Left Jostick Value: ","%f",gamepad1.left_stick_y);
            telemetry.addData("Lift Position", "%d ", robot.lift.getCurrentPosition());
            telemetry.addData("Slide", "%d ", robot.slide.getCurrentPosition());
            telemetry.addData("robot motor position", "%d", robot.leftFrontMotor.getCurrentPosition());
            telemetry.addData("MCMD", "%.1f %.1f", motion.vD, motion.vTheta);
            telemetry.addData("Rotation", "%f ", robot.getRobotAngle());
            telemetry.addData("PDIF Stuff:", "%s",robot.slide.getPIDFCoefficients(DcMotorEx.RunMode.RUN_TO_POSITION).toString());
            telemetry.addData("Front Left Power", "%f ", wheels.frontLeft);
            telemetry.addData("Front Right Power", "%f",wheels.frontRight);
            telemetry.addData("Back Right Power", "%f",wheels.backRight);
            telemetry.addData("Back Left Power", "%f",wheels.backLeft);
            telemetry.addData("Clear Count: ","%d",clearCount);
            telemetry.addData("Lift Target: ","%d",liftTarget);



            telemetry.update();

        }//end while(OpmodeIsActive);
        removePidVariable();
    }//end public void runopmode();

}//end class
