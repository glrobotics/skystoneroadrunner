//better autonomous code
package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.DcMotor;
import java.util.List;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaSkyStone;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.TfodSkyStone;

import java.util.ArrayList;

@Autonomous
public class SSAUTO1BlockBlueONEBLOCK extends LinearOpMode {

    Robot10785 robot = new Robot10785();

    public String found;
    public int target;
    //public int autonomousStep = 1;
    public String storedMineralposition;
    private ElapsedTime runtime = new ElapsedTime();
    private double currentTime = robot.period.seconds();
    public int diagnalLength = 34;
    public int LaW = 24;
    public double startTime = 0;
    public float superFast=0.9f;
    public float maxSpeed = 0.65f;//was 0.4
    public float slowSpeed=0.4f;
    public float accTime = 1.0f;
    private float reverseDistance = 0;
    public final int liftceil = 2226;
    public final int liftfloor = 119;
    public final int slidemax = 461;
    public final int slideOutBlock=205;
    public final int liftmin = -570;
    private VuforiaSkyStone vuforiaSkyStone = new VuforiaSkyStone();
    private TfodSkyStone tfodSkyStone = new TfodSkyStone();
    private final float k = -1.0f;//A constant made for ease of transferral
    public final float clawOpen=1.2f;
    public final float claw2Open=0f;
    public final float claw2Close=1.05f;
    public final float clawClose=0.6f;
    public final float hookDown=1.0f;
    public final float hook2Down=0f;
    public final float hookUp=0.44f;
    public final float hook2Up=0.5f;
    public int[] liftClearHeight={liftfloor,448,548,869,1115,1361,1610,1944};
    public int[] liftPlaceHeight={liftfloor,148,405,691,1034,1287,1490,1703};
    public int clearCount=0;

    //Back Left of robot compared to corner of building zone

    public enum AutonomousStep{
        moveFromHomePosition, strafeToFirstBlock, moveForwardToFirstBlock, lowerLift, grabBlock1, bringLiftUp1,
        moveBack1, turnToDrive1, moveUnderBar1, turnTowardsTray1, moveIntoTray1, releaseBlock1, moveAwayFromTray1,
        turnTowardsLoad1, moveBackToBlock2, turnToPickUpBlock2,moveForwardAndKnockBlock, moveToBlock2, sideWaysToLoadBlock,
        lowerLift2,grabBlock2, moveBack2, turnToTray2, moveToTray2, turnTowardsTray2, moveTowardsTray2,dropBlock2,moveToCenterTray,
        hookTray, moveToBuildZone, endAuto,turnToBuildZone,turnToBuildZone2,turnToBuildZone3,backToBuildZone,hookTrayPart2, liftHooks,
        strafeToPicture, turnToBlock2

    }
    public AutonomousStep autonomousStep = AutonomousStep.moveFromHomePosition;



    @Override
    public void runOpMode() {
        robot.init(hardwareMap);
        robot.setHomePositions();
        robot.startingAngle=0;
        robot.hook.setPosition(hookUp);
        robot.hook2.setPosition(hook2Up);

        telemetry.addData("Status", "Initialized 1");
        telemetry.update();


//        robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift.setTargetPosition(0);
        robot.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.lift.setPower(1);

//        robot.slide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.slide.setTargetPosition(0);
        robot.slide.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slide.setPower(1);

        robot.tape1.setTargetPosition(0);
        robot.tape2.setTargetPosition(0);
        robot.tape1.setPower(1);
        robot.tape2.setPower(1);

        // Sample TFOD Op Mode
        // Initialize Vuforia.
        vuforiaSkyStone.initialize(
                //*Sharp inhale*
                "AbQaekX/////AAABmdZK4VbDrU0cmz7SaHl/whRyPl7Ef/qgl6dy0r02zyIydswhIKnSJslloshmr7SR0dv9mi1bXIP2WrBUXMANqvWEVuEYCXUwPF2bxWiLRuzmmfJXPzusGPfVqJlYz5DPHoh+GXzErifqDn9pND1e8pxs5hCTdAwSAG4DeyMEhRXTuuLKKusNLKyDwoGjLR7ndnRoi5iQxFKRMnkpnY6XawJvQozMzIxP9NzKe3Sgyzr7Q+yh6AJyPOHEHz1Ftx3jvNb9U+n+l2rUlJxxlAwyAKf5/ugGo/T0BDtozghrwVDi6DTgNLzlbBIC4o8ly/UpF0/rPSt+hmMt0f+OA8yDe194IMDBf2VsAXethn52oh/e",
                //AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                hardwareMap.get(WebcamName.class, "Webcam 1"), // cameraName
                "", // webcamCalibrationFilename
                true, // useExtendedTracking
                true, // enableCameraMonitoring
                VuforiaLocalizer.Parameters.CameraMonitorFeedback.AXES, // cameraMonitorFeedback
                0, // dx
                0, // dy
                0, // dz
                0, // xAngle
                0, // yAngle
                0, // zAngle
                true); // useCompetitionFieldTargetLocations
        // Set min confidence threshold to 0.7
        tfodSkyStone.initialize(vuforiaSkyStone, 0.5F, true, true);
        // Initialize TFOD before waitForStart.
        // Init TFOD here so the object detection labels are visible
        // in the Camera Stream preview window on the Driver Station.
        tfodSkyStone.activate();


        //robot.lift2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        //robot.lift2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        double runningavg = 0;
        boolean skystonefound = false;
        int skystonenotfoundcount = 0;
        String stoneLoc=null;
        robot.claw.setPosition(0);
        robot.claw2.setPosition(claw2Close);
        while (!opModeIsActive() && !isStopRequested()) {
            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());


            telemetry.addData("status", "waiting for a start command...");
            if (tfodSkyStone != null) {
                // getUpdatedRecognitions() will return null if no new information is available since
                // the last time that call was made.
                List<Recognition> updatedRecognitions = tfodSkyStone.getUpdatedRecognitions();
                if (updatedRecognitions != null) {
                    skystonefound = false;
//                    telemetry.addData("# Object Detected", updatedRecognitions.size());
                    // step through the list of recognitions and display boundary info.
                    int i = 0;
                    for (Recognition recognition : updatedRecognitions) {
                        if (recognition.getLabel().equalsIgnoreCase("Skystone")) {
                            //telemetry.addData(String.format("label (%d)", i), recognition.getLabel());
                            runningavg = (recognition.getLeft() * 0.2) + (runningavg * 0.8);
                            skystonefound = true;
                            skystonenotfoundcount = 0;
//                            telemetry.addData(String.format("  left,top (%d)", i), "%.03f , %.03f",
//                                    recognition.getLeft(), recognition.getTop());
                        }
                    }
                }
            }
            if (!skystonefound) {
                skystonenotfoundcount++;
            }
            if (skystonenotfoundcount > 200) {//The Blue side is a little flakey, keep this at 200
                stoneLoc = "Load";//Right?
            } else if (runningavg > 300) {
                stoneLoc = "Center";//Duh
            } else {
                stoneLoc = "Build";//Left?
            }
            robot.fieldX=96;
            robot.fieldY=0;
            telemetry.addData("Robot X Coordinate: ","%f",robot.fieldX);
            telemetry.addData("Robot Y Coordinate: ","%f",robot.fieldY);

            telemetry.addData("Skystone location: ",stoneLoc);
            telemetry.addData("  Running avg: ",  "%f",runningavg);
            telemetry.addData("Skystone not found", "%d", skystonenotfoundcount);
            telemetry.update();
        }
        //waitForStart();

        while (opModeIsActive()) {


            switch (autonomousStep) {
                case moveFromHomePosition:// LIFT LEVEL 1/Open claw/Move forward
                    robot.lift.setTargetPosition(liftClearHeight[0]);
                    robot.claw.setPosition(clawOpen);
                    robot.claw2.setPosition(claw2Open);
                    robot.moveHoldAngle(20f,slowSpeed,0);
                    autonomousStep = AutonomousStep.strafeToFirstBlock;
                    break;

                case strafeToFirstBlock://STRAFES TO LINE UP WITH CORRECT SKYSTONE
                    if(robot.moveComplete()) {
                        if (stoneLoc.equals("Build")) {
                            robot.strafeHoldAngle(90.0f,-slowSpeed*k,0);
                        }
                        else if (stoneLoc.equals("Center")) {
                            robot.strafeHoldAngle(94.5f,-slowSpeed*k,0);
                        }
                        else {
                            robot.strafeHoldAngle(106.5f,slowSpeed*k,0);
                        }
                        autonomousStep = AutonomousStep.moveForwardToFirstBlock;
                    }
                    break;

                case moveForwardToFirstBlock://MOVES FORWARD TO 1ST BLOCK
                    if(robot.moveComplete()){
                        robot.moveHoldAngle(25f,slowSpeed,0);
                        autonomousStep=AutonomousStep.lowerLift;
                    }
                    break;

                case lowerLift:
                    if (robot.moveComplete()) {
                        robot.lift.setTargetPosition(liftClearHeight[0]);
                        autonomousStep = AutonomousStep.grabBlock1;
                    }
                    break;

                case grabBlock1:
                    if (robot.lift.getCurrentPosition() < 140) {
                        robot.claw.setPosition(clawClose);
                        robot.claw2.setPosition(claw2Close);
                        robot.slide.setTargetPosition(slideOutBlock);//max is 127
                        startTime = robot.period.seconds();
                        autonomousStep = AutonomousStep.bringLiftUp1;
                    }
                    break;


                case bringLiftUp1://BRINGS LIFT UP TO 60
                    if (robot.period.seconds() - startTime > 0.5) {
                        robot.lift.setTargetPosition(133);
                        autonomousStep=AutonomousStep.moveBack1;
                    }
                    break;

                case moveBack1: // MOVES BACKWARDS AFTER GRABBING BLOCK
                    if (Math.abs(robot.lift.getCurrentPosition()-133) < 10) {
                        robot.moveHoldAngle(24.0f,-maxSpeed,0); // was 12
                        autonomousStep = AutonomousStep.turnToDrive1;
                    }
                    break;

                case turnToDrive1:
                    if (robot.moveComplete()){
                        robot.turn(-80,slowSpeed);
                        autonomousStep=AutonomousStep.moveUnderBar1;
                    }
                    break;

                case moveUnderBar1:
                    if (robot.moveComplete()){
                        startTime=robot.period.seconds();
                        if (stoneLoc.equals("Build")) {
                            robot.moveHoldAngle(42.0f,superFast,270);
                        } else if (stoneLoc.equals("Center")) {
                            robot.moveHoldAngle(38.0f,superFast,270);
                        } else {
                            robot.moveHoldAngle(16.5f,superFast,270);
                        }
                        autonomousStep=AutonomousStep.turnTowardsTray1;
                    }
                    break;
                case turnTowardsTray1:
                    if (robot.period.seconds()-startTime>=0.5){
                        robot.lift.setTargetPosition(liftClearHeight[1]);
                        robot.slide.setTargetPosition(slideOutBlock);
                    }
                    if (robot.moveComplete()){
                        robot.turn(0,slowSpeed);
                        autonomousStep=AutonomousStep.moveIntoTray1;
                    }
                    break;

                case moveIntoTray1: // LIFT UP AND MOVE SLIDE OUT AND MOVE FORWARD 10 INCHES

                    if (robot.moveComplete()&&(robot.lift.getCurrentPosition()-448<=5)) {
                        robot.moveHoldAngle(27.5f,slowSpeed,0);
                        autonomousStep = AutonomousStep.releaseBlock1;
                    }
                    break;

                case releaseBlock1:// OPEN CLAW/RELEASE BLOCK
                    if  (robot.moveComplete()) {
                        robot.claw.setPosition(clawOpen);
                        robot.claw2.setPosition(claw2Open);
                        robot.lift.setTargetPosition(liftClearHeight[2]);
                        startTime=robot.period.seconds();
                        autonomousStep = AutonomousStep.moveToCenterTray;
                    }
                    break;

                case moveToCenterTray://CLOSE CLAW
                    if (robot.period.seconds()-startTime>0.3){
                        if (stoneLoc.equals("Load")){
                            robot.strafeHoldAngle(26.0f, maxSpeed, 0);
                        }
                        else {
                            robot.strafeHoldAngle(26.0f, maxSpeed, 0);
                        }
                        robot.slide.setTargetPosition(0);
                        robot.lift.setTargetPosition(liftClearHeight[3]);
                        // robot.slide.setTargetPosition(250);
                        autonomousStep=AutonomousStep.hookTray;
                    }
                    break;
                case hookTray: //MOVE FORWARD 4 INCHES AND SET LIFT POSITION TO 0 AND EXTEND TAPE AND MOVE SLIDE IN
                    if(robot.moveComplete()){
                        robot.moveHoldAngle(31f,slowSpeed,0);
                        robot.claw.setPosition(clawClose);
                        robot.claw2.setPosition(claw2Close);
                        autonomousStep = AutonomousStep.hookTrayPart2;
                    }
                    break;
                case hookTrayPart2:
                    if(robot.moveComplete()) {
                        startTime=robot.period.seconds();
                        robot.hook.setPosition(hookDown);
                        robot.hook2.setPosition(hook2Down);
                        autonomousStep=AutonomousStep.backToBuildZone;
                    }
                    break;

                case backToBuildZone: //MOVE BACK
                    if (robot.period.seconds()-startTime>=1){
                        robot.moveHoldAngle(5,-maxSpeed,-15);
                        autonomousStep = AutonomousStep.turnToBuildZone3;
                    }
                    break;

                    //ADD THREE SEPERATE STEPS, 5 DEGREES, BACK, 85 DEGREES
                case turnToBuildZone: //not used
                    if(robot.moveComplete()){
                        robot.turn(-10,slowSpeed);
                        autonomousStep= AutonomousStep.turnToBuildZone2;
                    }
                    break;
                case turnToBuildZone2: //not used
                    if(robot.moveComplete()){
                        robot.moveHoldAngle(0,slowSpeed,350);
                        autonomousStep=AutonomousStep.turnToBuildZone3;
                    }
                    break;
                case turnToBuildZone3:
                    if(robot.moveComplete()) {
                        robot.turn(-90,slowSpeed);
                        autonomousStep = AutonomousStep.moveToBuildZone;
                    }
                    break;
                case moveToBuildZone:
                    if (robot.moveComplete()){
                        robot.moveHoldAngle(25,slowSpeed,270);
                        autonomousStep=AutonomousStep.liftHooks;
                    }
                    break;

                case liftHooks:
                    if (robot.moveComplete()){
                        robot.hook.setPosition(hookUp);
                        robot.hook2.setPosition(hook2Up);
                        startTime = robot.period.seconds();
                        autonomousStep=AutonomousStep.strafeToPicture;
                    }
                    break;

                case strafeToPicture:
                    if (robot.period.seconds() - startTime > .5){
                        robot.strafeHoldAngle(18, -maxSpeed, 270);
                        autonomousStep=AutonomousStep.moveBackToBlock2;
                    }
                    break;

                case moveBackToBlock2:
                    if (robot.moveComplete()){
                        robot.moveHoldAngle(108, -maxSpeed, 270);
                        robot.lift.setTargetPosition(0);
                        robot.claw.setPosition(clawOpen);
                        robot.claw2.setPosition(claw2Open);
                        autonomousStep=AutonomousStep.turnToBlock2;
                    }
                    break;

                 case turnToBlock2:
                     if (robot.moveComplete()) {
                         robot.turn(355, slowSpeed);
                         autonomousStep=AutonomousStep.moveToBlock2;
                     }
                     break;

                case moveToBlock2:
                    if (robot.moveComplete()) {
                        robot.moveHoldAngle(20, maxSpeed, 0);
                        autonomousStep=AutonomousStep.endAuto;
                    }
                    break;
                }


            robot.moveUpdate();
            telemetry.addData("Status", "Running");
            telemetry.addData("MoveStep: ", "%s", robot.moveStep);
            telemetry.addData("Robot X Coordinate: ","%f",robot.fieldX);
            telemetry.addData("Robot Y Coordinate: ","%f",robot.fieldY);
            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.addData("AutonomousStep", "%s", autonomousStep.name());
            telemetry.addData("Found", "%s", found);
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            //telemetry.addData("Pos (in)", "{X, Y, Z} = %.1f, %.1f, %.1f",
            telemetry.addData("armPosition", "%d ", robot.lift.getCurrentPosition());
            telemetry.addData("robot motor position", "%d", robot.leftFrontMotor.getCurrentPosition());
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());
            telemetry.update();

        }
    }
}
