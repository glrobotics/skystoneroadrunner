package org.firstinspires.ftc.teamcode.drive.opmode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.path.heading.ConstantInterpolator;
import com.acmerobotics.roadrunner.path.heading.HeadingInterpolator;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.acmerobotics.roadrunner.trajectory.TrajectoryBuilder;
import com.acmerobotics.roadrunner.trajectory.TrajectoryGenerator;
import com.acmerobotics.roadrunner.trajectory.constraints.MecanumConstraints;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.drive.DriveConstants;
import org.firstinspires.ftc.teamcode.drive.mecanum.SampleMecanumDriveBase;
import org.firstinspires.ftc.teamcode.drive.mecanum.SampleMecanumDriveREV;

/*
 * This is an example of a more complex path to really test the tuning.
 */
@Autonomous(group = "drive")
public class SplineTest extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        SampleMecanumDriveBase drive = new SampleMecanumDriveREV(hardwareMap);
        MecanumConstraints constraints=new MecanumConstraints(DriveConstants.BASE_CONSTRAINTS,DriveConstants.TRACK_WIDTH,8.5);
        waitForStart();

        if (isStopRequested()) return;

        drive.followTrajectorySync(drive.trajectoryBuilder().splineTo(new Pose2d (30,-2, 0)).build());//move to first center block

        drive.followTrajectorySync(drive.trajectoryBuilder().reverse().

                splineTo(new Pose2d (26,32, -90)).// first point
                splineTo(new Pose2d (26,56,-45)).//second Point
                splineTo(new Pose2d (10,74,0)).build());// final destination

        drive.followTrajectorySync(drive.trajectoryBuilder().forward(18).build());

        sleep(1000);

        drive.followTrajectorySync(drive.trajectoryBuilder().back(30).build());

        sleep(1000);

                drive.followTrajectorySync(drive.trajectoryBuilder().splineTo(new Pose2d(0,0,-90)).

                splineTo(new Pose2d(30,-26,0)).build());

        sleep(10000);
      //  drive.followTrajectorySync(drive.trajectoryBuilder().reverse().splineTo(new Pose2d (0,0, 0)).build());

    }
}
